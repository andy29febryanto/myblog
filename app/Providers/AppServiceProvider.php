<?php

namespace App\Providers;
use Illuminate\Contracts\Events\Dispatcher;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;
use Auth;
use App\Role;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        $events->listen(BuildingMenu::class, function (BuildingMenu $event)
        {
            $id_role = Auth::user()->id_role;
            $data = Role::where('id_role', $id_role)->first();
            $role_name = $data['role_name'];
           $event->menu->add('MENU');
           if ($role_name=="admin") {
               $event->menu->add(
                   [
                    'text' => 'User',
                    'url' => 'user',
                    'icon' => 'fas fa-fw fa-users'
                   ],
                   [
                    'text' => 'Type User',
                    'url' => 'role',
                    'icon' => 'fas fa-fw fa-user-tie'
                   ]
                );
           } else if ($role_name=="editor"){
               $event->menu->add(
                   [
                    'text' => 'Kategori Artikel',
                    'url' => 'kakel',
                    'icon' => 'fas fa-fw fa-file-pdf'
                   ],
                   [
                    'text' => 'Artikel',
                    'url' => 'artikel',
                    'icon' => 'fas fa-fw fa-history'
                   ]
                   );
           } else if ($role_name=="penulis"){
                $event->menu->add(
                    [
                    'text' => 'Artikel',
                    'url' => 'artikel',
                    'icon' => 'fas fa-fw fa-history'
                    ]
                );  
           }
        });
    }
}
