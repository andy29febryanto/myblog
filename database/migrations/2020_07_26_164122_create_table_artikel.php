<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableArtikel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artikel', function (Blueprint $table) {
            $table->increments('id_artikel');
            $table->unsignedBigInteger('id_penulis');
            $table->unsignedBigInteger('id_editor');
            $table->unsignedBigInteger('id_kategori');
            $table->string('judul');
            $table->string('isi_artikel');
            $table->timestamp('email_verified_at')->nullable();
            $table->set('status',['Publish','Waiting List', 'Reject' ,'Draft']);	
            $table->timestamps();

            $table->foreign('id_kategori')->references('id_kategori')->on('kakel');
            $table->foreign('id_penulis')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artikel');
    }
}
