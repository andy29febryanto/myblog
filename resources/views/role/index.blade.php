@extends('adminlte::page')

@section('title','role')

@section('content_header')
<h1 class="m-0 text-dark">Manajemen role</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-header">
                    <a href="{{ route('role.create') }}" class="btn btn-primary btn-md">
                        <i class="fa fa-plus"> Tambah</i>
                    </a>       
                </div>
                <div class="card-body">
                    <table class="display table table-bordered table-striped" id="example">
                        <thead>
                            <tr>
                                <th style="width: 20px">#</th>
                                <th> nama role</th>
                                <th style="width: 80px"> Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no= 1 ; ?>
                            @forelse ($data as $item)
                                <tr>
                                    <td>
                                        {{ $no }}
                                    </td>
                                    <td>
                                        {{ $item->role_name }}
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('role.edit', $item->id_role) }}" class="btn btn-success">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                            <a onclick="hapus('{{ $item->id_role }}')" href="#" class="btn btn-primary">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <?php $no++; ?>
                            @empty
                                <tr>
                                    <td colspan="6">
                                        Tidak Ada Data
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
              
            </div>
        </div>
    </div>
    @stop
    @section('plugins.Datatables',true)
    @section('plugins.Sweetalert2', true)
    @section('plugins.Pace',true)
    
    @section('js')
<script>
    $(document).ready(function() {
        $('#example').DataTable( {
            "paging":   true,
            "pagelength": 10,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "ordering": false,
            "info":     false,
            "language": {
                "zeroRecords": "No entries found. Please adjust your search parameters.",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": "No entries found"
            },
        } );
        
    } );
    
</script>
        @if (session('success'))
            <script type="text/javascript">
                Swal.fire(
                    'Sukses!',
                    '{{ session('success') }}',
                    'success'
                )
            </script>
        @endif
            <script type="text/javascript">
                function hapus(id){
                    Swal.fire({
                        title : 'Konfirmasi',
                        text : "Yakin ingin menghapus data ini ?",
                        icon : 'warning',
                        showCancelButton : true,
                        confirmButtonColor : '#3085d6',
                        cancelButtonColor: '#dd3333',
                        confirmButtonText: 'Hapus',
                        cancelButtonText: 'Batal',
                    }).then((result) =>  {
                        if (result.value){
                            $.ajax({
                                url: "/role/"+id,
                                type: 'DELETE',
                                data:{
                                    '_token' : $('meta[name=csrf-token]').attr("content"),
                                },
                                success : function(result){
                                    Swal.fire(
                                        'Sukses!',
                                        'Berhasil dihapus',
                                        'success'
                                    );
                                    location.reload();
                                }
                            })
                        }
                    })
                }
            </script>
            @stop