<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Auth;
use App\Role;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_role','foto','name', 'email', 'password',
    ];
    protected $table = "users";
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function adminlte_image()
    {
        $foto = Auth::user()->foto;
        return '/assets/picture/'.$foto;
    }

    public function adminlte_desc()
    {
        $id_role = Auth::user()->id_role;
        $role_name = Role::findOrfail($id_role);
        return 'Tipe User : '.$role_name['role_name'];
    }

    public function adminlte_profile_url()
    {
        $id= Auth::user()->id;
        return 'profil/'.$id;
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
{
    return $this->belongsTo('App\Role', 'id_role');
}



}
