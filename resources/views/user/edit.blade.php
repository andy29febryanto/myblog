@extends('adminlte::page')
@section('title','user')
@section('content_header')
    <h1 class="m-0 text-dark">Manajemen user</h1>
@stop
@section('content')
    <div class="row">
        <div class="col-12">
            @if ($errors->any())
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" data-dismiss="alert" class="close" aria-hidden="true">x</button>
                    <h4><i class="icon fa fa-warning"></i>Perhatian!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
            <div class="card">
                <div class="card-header">
                    Tambah user
                </div>
                <div class="card-body">
                    <form action="{{ route('user.update',$id ?? '') }}" method="post" class="form-horizontal">
                        @method('PUT')
                        {{ csrf_field() }}

<div class="form-group">
    <label for="nama" class="col-sm-2 cotrol-label">Nama</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="name" value="{{ $name ?? ''  }}">
    </div>
</div>

<div class="form-group">
    <label for="email" class="col-sm-2 cotrol-label">Email</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="email" value="{{ $email ?? ''  }}">
    </div>
</div>
<div class="form-group">
    <label for="password" class="col-sm-2 cotrol-label">Password</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" placeholder="jika tidak ingin diubah,tak perlu di isi" name="password" value="{{ $password ?? ''  }}">
    </div>
</div>
<div class="form-group">
    <label for="hak_akses" class="col-sm-2 cotrol-label">Hak Akses</label>
    <div class="col-sm-10">
        <select name="hak_akses" class="form-control">
            @foreach ($role as $item)
                <option value="{{ $item->id_role }}" {{ ( ($hak_akses ?? ''   ) == $item->role_name ) ? 'selected' : '' }}>
                   {{ $item->role_name }}
                </option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        <input type="submit" class="btn btn-md btn-success" name="simpan" value="Simpan">
        <a href="{{ route('user.index') }}" role="button" class="btn btn-primary">Batal</a>
    </div>
</div>
                    </form>
                </div>
            </div>

        </div> 
    </div>
    @stop
    @section('plugins.Pace',true)