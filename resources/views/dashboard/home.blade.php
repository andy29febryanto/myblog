@extends('beranda')
@section('title')
MyBlog-Dashboard    
@endsection
@section('content')
  <style> 
    @media (min-width: 768px) {

      /* show 3 items */
      .cinner .active,
      .cinner .active + .crsitem,
      .cinner .active + .crsitem + .crsitem,
      .cinner .active + .crsitem + .crsitem + .crsitem  {
          display: block;
      }
      
      .cinner .crsitem.active:not(.crsitem-right):not(.crsitem-left),
      .cinner .crsitem.active:not(.crsitem-right):not(.crsitem-left) + .crsitem,
      .cinner .crsitem.active:not(.crsitem-right):not(.crsitem-left) + .crsitem + .crsitem,
      .cinner .crsitem.active:not(.crsitem-right):not(.crsitem-left) + .crsitem + .crsitem + .crsitem {
          transition: none;
      }
      
      .cinner .crsitem-next,
      .cinner .crsitem-prev {
        position: relative;
        transform: translate3d(0, 0, 0);
      }
      
      .cinner .active.crsitem + .crsitem + .crsitem + .crsitem + .crsitem {
          position: absolute;
          top: 0;
          right: -25%;
          z-index: -1;
          display: block;
          visibility: visible;
      }
      
      /* left or forward direction */
      .active.crsitem-left + .crsitem-next.crsitem-left,
      .crsitem-next.crsitem-left + .crsitem,
      .crsitem-next.crsitem-left + .crsitem + .crsitem,
      .crsitem-next.crsitem-left + .crsitem + .crsitem + .crsitem,
      .crsitem-next.crsitem-left + .crsitem + .crsitem + .crsitem + .crsitem {
          position: relative;
          transform: translate3d(-100%, 0, 0);
          visibility: visible;
      }
      
      /* farthest right hidden item must be abso position for animations */
      .cinner .crsitem-prev.crsitem-right {
          position: absolute;
          top: 0;
          left: 0;
          z-index: -1;
          display: block;
          visibility: visible;
      }
      
      /* right or prev direction */
      .active.crsitem-right + .crsitem-prev.crsitem-right,
      .crsitem-prev.crsitem-right + .crsitem,
      .crsitem-prev.crsitem-right + .crsitem + .crsitem,
      .crsitem-prev.crsitem-right + .crsitem + .crsitem + .crsitem,
      .crsitem-prev.crsitem-right + .crsitem + .crsitem + .crsitem + .crsitem {
          position: relative;
          transform: translate3d(100%, 0, 0);
          visibility: visible;
          display: block;
          visibility: visible;
      }
      
      }
      }
</style> 

<section class="artikel">
  <div class="container-fluid">
      <div class="col-md-12">
          <h2 style="text-align:center ;color:#42F0CD; padding-top:50px;" class="wow bounce slow"><i class="fas fa-crooshair"></i> <b>Artikel</b></h2>
        </div>
      
      @foreach ($data as $item)
    
      <div class="row">
      <div class="col">
        <div class="card ">
          
          <div class="card-body">
            <h5 class="card-title">{{$item->judul}}</h5>
              <h6 class="card-subtitle">Kategori: <a href="{{ route('dtlCategory',$item->kakel->id_kategori) }}"> {{ $item->kakel->nama_kategori}}</a> </h6><br>
            <p class="card-text text-center">{{  \Illuminate\Support\Str::limit(strip_tags($item->isi_artikel),100) }}
              @if (strlen(strip_tags($item->isi_artikel)) > 100)
                <br><br><a href="{{ route('dtlArticle',$item->slug_judul) }}" id="readmore" class="btn btn-info btn-md bg-success" role="button">Read More</a>
              @endif</p>
              <span class="card-link text-left">Penulis : <a href="{{ route('dtlWritter',$item->penulis->id) }}">{{$item->penulis->name}}</a> </span>
              <span  class="card-link">Editor : {{$item->editor->name}}</span>
          </div>
              <div class="card-footer text-muted">
                {{ Carbon\Carbon::parse($item->updated_at)->diffForHumans()  }}
              </div>
          </div>
      </div>
    </div>
    <br>
      @endforeach
      <div class="row">
        <br>
          <div class="Page card">
              {{ $data->links() }}
          </div>
      </div>
      <br>
    </div>  
  </div>
</section>

@endsection
@section('js_visitor')
    
@endsection