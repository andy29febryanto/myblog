{{ csrf_field() }}

<div class="form-group">
    <label for="nama" class="col-sm-2 cotrol-label">Nama Role</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="role_name" value="{{ $role_name ?? ''  }}">
    </div>
</div>

<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        <input type="submit" class="btn btn-md btn-success" name="simpan" value="Simpan">
        <a href="{{ route('role.index') }}" role="button" class="btn btn-primary">Batal</a>
    </div>
</div>