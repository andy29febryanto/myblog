@extends('adminlte::page')

@section('title','artikel')

@section('content_header')
<h1 class="m-0 text-dark">Artikel</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-header">
                    @if (Auth::user()->id_role=="3")
                        
                    <a href="{{ route('artikel.create') }}" class="btn btn-primary btn-md">
                        <i class="fa fa-plus"> Tambah</i>
                    </a>       
                    @endif
                </div>
                    
                <div class="card-body">
                    <table class="display table table-bordered table-striped" style="width: 100%;" id="example">
                        <thead>
                            <tr>
                                <th style="width: 20px"></th>
                                
                                <th> penulis</th>
                                <th> </th>
                                <th> </th>
                                <th> status</th>
                                <th> editor</th>
                                <th style="width: 80px"> </th>
                            </tr>
                        </thead>
                        <thead>
                            <tr>
                                <th style="width: 20px">#</th>
                                
                                <th> penulis</th>
                                <th> judul</th>
                                <th> isi artikel</th>
                                <th> status</th>
                                <th> editor</th>
                                <th style="width: 80px"> Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no= 1 ; ?>
                            @foreach ($data as $item)
                                <tr>
                                    <td>
                                        {{ $no }}
                                    </td>
                                    <td>
                                        {{ $item->penulis->name }}
                                    </td>
                                    <td>
                                        {{ $item->judul }}
                                    </td>
                                    <td>
                                        {{ $item->isi_artikel }}
                                    </td>
                                    <td>
                                        {{ $item->status }}
                                    </td>
                                    <td>
                                        {{ $item->editor->name }}
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('artikel.edit', $item->id_artikel) }}" class="btn btn-success">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                            <a onclick="hapus('{{ $item->id_artikel }}')" href="#" class="btn btn-primary">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <?php $no++; ?>
                                @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    @stop
    @section('plugins.Datatables',true)
    @section('plugins.Sweetalert2', true)
    @section('plugins.Pace',true)
    @section('js')
<script>
    $(document).ready(function() {
        $('#example').DataTable( {
            "paging":   true,
            "pagelength": 10,
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "ordering": false,
            "info":     false,
            "language": {
                "zeroRecords": "No entries found. Please adjust your search parameters.",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": "No entries found"
            },
            "stateSave": true,
            "stateDuration": 3600,
            initComplete: function () {
                this.api().columns(['4','1','5']).every( function () {
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $(column.header()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );
     
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                } );
            }
        
        
        } );
        
    } );
    
</script>
        @if (session('success'))
            <script type="text/javascript">
                Swal.fire(
                    'Sukses!',
                    '{{ session('success') }}',
                    'success'
                )
            </script>
        @endif
            <script type="text/javascript">
                function hapus(id){
                    Swal.fire({
                        title : 'Konfirmasi',
                        text : "Yakin ingin menghapus data ini ?",
                        icon : 'warning',
                        showCancelButton : true,
                        confirmButtonColor : '#3085d6',
                        cancelButtonColor: '#dd3333',
                        confirmButtonText: 'Hapus',
                        cancelButtonText: 'Batal',
                    }).then((result) =>  {
                        if (result.value){
                            $.ajax({
                                url: "/artikel/"+id,
                                type: 'DELETE',
                                data:{
                                    '_token' : $('meta[name=csrf-token]').attr("content"),
                                },
                                success : function(result){
                                    Swal.fire(
                                        'Sukses!',
                                        'Berhasil dihapus',
                                        'success'
                                    );
                                    location.reload();
                                }
                            })
                        }
                    })
                }
            </script>
       
            @stop