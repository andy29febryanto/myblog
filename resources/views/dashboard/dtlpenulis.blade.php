@extends('beranda')
@section('title')
MyBlog-Dashboard    
@endsection
@section('content')

<div class="row  mt-5 pt-3 pb-2 ">
    <div class="col-md-6 pl-5 ml-5">
        <div class="d-flex flex-row border rounded">
              <div class="p-0 w-25">
                  <img src="/assets/picture/{{$penulis->foto}}" class="img-thumbnail border-0" />
              </div>
              <div class="pl-3 pt-2 pr-2 pb-2 w-75 border-left">
                      <h4 class="text-primary">{{ $penulis->name}}</h4>
                      <h5 class="text-info">Photographer</h5>
                      <ul class="m-0 float-left" style="list-style: none; margin:0; padding: 0">
                          <li><i class="fab fa-envelope"></i> {{$penulis->email}}</li>
                      </ul>                   
                </div>
        </div>
    </div>
</div>
{{--  //endprofil  --}}
<section class="artikel">
    <div class="container-fluid">
        <div class="col-md-12">
            <h2 style="text-align:center ;color:#42F0CD; padding-top:50px;" class="wow bounce slow"><i class="fas fa-crooshair"></i> <b>Artikel</b></h2>
          </div>
        
        @foreach ($data as $item)
      
        <div class="row">
        <div class="col">
          <div class="card ">
            
            <div class="card-body">
              <h5 class="card-title">{{$item->judul}}</h5>
                <h6 class="card-subtitle">Kategori: <a href="{{ route('dtlCategory',$item->kakel->id_kategori) }}"> {{ $item->kakel->nama_kategori}}</a> </h6><br>
              <p class="card-text text-center">{{  \Illuminate\Support\Str::limit(strip_tags($item->isi_artikel),100) }}
                @if (strlen(strip_tags($item->isi_artikel)) > 100)
                  <br><br><a href="{{ route('dtlArticle',$item->slug_judul) }}" id="readmore" class="btn btn-info btn-md bg-success" role="button">Read More</a>
                @endif</p>
                <span class="card-link text-left">Penulis : <a href="{{ route('dtlWritter',$item->penulis->id) }}">{{$item->penulis->name}}</a> </span>
                <span  class="card-link">Editor : {{$item->editor->name}}</span>
            </div>
                <div class="card-footer text-muted">
                  {{ Carbon\Carbon::parse($item->updated_at)->diffForHumans()  }}
                </div>
            </div>
        </div>
      </div>
      <br>
        @endforeach
        <div class="row">
          <br>
            <div class="Page card">
                {{ $data->links() }}
            </div>
        </div>
        <br>
      </div>  
    </div>
    <div class="row ml-3 mb-3">
        <div class="col">
            <a href="{{ route('penulis') }}" class=" btn bg-danger btn-danger">Kembali</a>

        </div>
    </div>
  </section>
  

@stop