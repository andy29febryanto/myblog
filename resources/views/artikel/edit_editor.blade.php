@extends('adminlte::page')
@section('title','Artikel')
@section('content_header')
    <h1 class="m-0 text-dark">Artikel</h1>
@stop
@section('content')
    <div class="row">
        <div class="col-12">
            @if ($errors->any())
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" data-dismiss="alert" class="close" aria-hidden="true">x</button>
                    <h4><i class="icon fa fa-warning"></i>Perhatian!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
            <div class="card">
                <div class="card-header">
                    Edit Artikel
                </div>
                <div class="card-body">
                    <form action="{{ route('artikel.update',$id_artikel ?? '') }}" method="post" class="form-horizontal">
                        @method('PUT')
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="editor" class="col-sm-2 cotrol-label">Editor</label>
                            <div class="col-sm-10">
                                <select name="id_penulis" class="form-control">
                                    @foreach ($penulis as $item)
                                        <option value="{{ $item->id }}" {{ ( ($id_penulis ?? ''   ) == $item->id ) ? 'selected' : '' }}>
                                            {{ $item->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="editor" class="col-sm-2 cotrol-label">Editor</label>
                            <div class="col-sm-10">
                                <select name="id_editor" class="form-control">
                                    @foreach ($editor as $item)
                                        <option value="{{ $item->id }}" {{ ( ($id_editor ?? ''   ) == $item->id ) ? 'selected' : '' }}>
                                            {{ $item->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>     

                        <div class="form-group">
                            <label for="judul" class="col-sm-2 cotrol-label">Judul</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="judul" value="{{ $judul ?? ''  }}">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="kategori" class="col-sm-2 cotrol-label">kategori artikel</label>
                            <div class="col-sm-10">
                                <select name="id_kategori" class="form-control">
                                    @foreach ($kakel as $item)
                                        <option value="{{ $item->nama_kategori }}" {{ ( ($id_kategori ?? ''   ) == $item->id_kategori ) ? 'selected' : '' }}>
                                            {{ $item->nama_kategori }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="isi">isi</label>
                            <textarea name="isi_artikel"  cols="70" rows="7">{!! $isi_artikel ?? '' !!}</textarea>    
                        </div>

<div class="form-group">
    <label for="Status">Status</label>
                        <select class="form-control" name="status">
                            <option value="Draft" @if ($status ?? '' == "Draft" )
                                selected>
                                Draft
                            </option>
                            <option value="Waiting List">Waiting List</option>
                            @else
                            <option value="Waiting List" selected>Waiting List</option>
                            <option value="Draft">Draft</option>
                            
                            @endif
                        </select>
                    </div>
                         
<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        <input type="submit" class="btn btn-md btn-success" name="simpan" value="Simpan">
        <a href="{{ route('artikel.index') }}" role="button" class="btn btn-primary">Batal</a>
    </div>
</div>
                    </form>
                </div>
            </div>

        </div> 
    </div>
    @stop
    @section('plugins.Pace',true)
    @section('js')
    <script src="/vendor/ckeditor/ckeditor.js"></script>
     <script>
        CKEDITOR.replace('isi_artikel', {
            width: '90%',
            height: 360
          });
        </script>
    @stop