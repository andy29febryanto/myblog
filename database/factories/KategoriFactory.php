<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use Faker\Generator as Faker;
use App\Kakel;

$factory->define(Kakel::class, function (Faker $faker) {
    return [
        'nama_kategori' => $faker->text,
    ];
});
