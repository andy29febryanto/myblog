<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kakel;
class KakelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Kakel::get();
        
     
        $tampil['data'] = $data;

        // dd($tampil);
        return view('kakel.index', $tampil);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['kakel'] = Kakel::get();
        
        return view('kakel.create',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama_kategori' => 'required'
            ]); 
            
    
    //buat data Kakel
    $dataKakel = Kakel::create($request->all());
 
    return redirect()->route("kakel.index")->with(
    "success",
    "Data berhasil disimpan."
     );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Kakel::findOrFail($id);
        
        return view("kakel.edit", $data);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            //validasi inputan
            $this->validate($request, [
                'nama_kategori' => 'required'
                ]);
               
                $dataKakel = Kakel::findOrFail($id);
                $dataKakel->nama_kategori = $request->nama_kategori;
        
                $dataKakel->save();
            
            return redirect()->route("kakel.index")->with(
            "success",
            "Data berhasil diubah."
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dataKakel = Kakel::findOrFail($id);
        $dataKakel->delete();
    }
}
