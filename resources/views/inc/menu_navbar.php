<nav id="scroll-ku" class="navbar navbar-white navbar-expand-lg fixed-top navbar-custom ">
		<div class="container">
			<a class="navbar-brand" href="#banner">
				My Blog
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<i class="fa fa-align-right" style="color:white;"></i>
			</button>
				<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
					<a class="nav-link" href="/">Beranda</a>
					</li>
					<li class="nav-item">
					<a  class="nav-link" href="/kategori">Kategori Artikel</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="/penulis">Penulis</a>
					</li>
				</ul>
				</div>
		</div>
	  </nav>