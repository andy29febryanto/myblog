<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Artikel;
use Faker\Generator as Faker;

$factory->define(Artikel::class, function (Faker $faker) {
    return [
        'judul' => $faker->text ,
        'slug_judul' => $faker->slug ,
        'id_kategori' => '1',
        'isi_artikel' => $faker->paragraph,
       'status' => 'Draft',
       'id_penulis' => '3',
        'id_editor' => '2'
    ];
});
