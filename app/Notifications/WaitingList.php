<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class WaitingList extends Notification
{
    use Queueable;

    protected $my_notification; 


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($msg)
    {   
        $this->my_notification = $msg; 
    }
    public function via($notifiable)
    {
        return ['mail'];

    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->line($this->my_notification)
        ->action('Click Here', url('artikel'))
        ->line('There is some new article you must check');
        
    }

    public function toDatabase($notifiable)
    {
        return [
          //
        ];
    }

    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
