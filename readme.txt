APLIKASI MYBLOG

System Requirements :
1. Laravel 7.*
2. PHP 7.2.27
3. Mysql / MariaDB
4. Composer 1.8.6

Nama Database : myblog_db;
Lokasi file db : myblog/myblog_db.sql;

Plugins :
1. Jeroennoeten/Laravel
2. Admin LTE
3. Datatables
4. Jquery
5. CkEditor

Cara Login ke administrator panel:
1. User 1 as Admin : 
Email : admin@gmail.com
Password : adminadmin
2. User 2 as Editor : 
Email : andy29febryanto@gmail.com
Password : editor123
3. User 3 as Penulis : 
Email : febriantoandi751@gmail.com
Password : adminadmin

Perubahan Status Artikel menjadi 4 kategori :
1. Publish
Untuk Artikel yang telah di confirm oleh editor untuk dipublikasikan.
2. Waiting List
Untuk Artikel yang telah dikirim oleh penulis, menunggu untuk konfirmasi dari editor
3. Reject
Untuk Artikel yang ditolak oleh editor
4. Draft
untuk Artikel yang masih belum selesai dikerjakan oleh penulis