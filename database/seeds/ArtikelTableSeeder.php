<?php

use Illuminate\Database\Seeder;
use App\Artikel;
class ArtikelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 10;
        factory(Artikel::class, $count)->create();
    }
}
