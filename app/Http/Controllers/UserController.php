<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = User::get();
        
     
        $tampil['data'] = $data;

        // dd($tampil);
        return view('user.index', $tampil);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['user'] = User::get();
        $data['hak_akses'] = Role::get();
    //    dd($data);
            return view('user.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           $this->validate($request, [
            'name' => 'required',
            'hak_akses' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            ]); 
            
   //enkripsi password
    $enkripsi = Hash::make($request->password);
    $request->merge(['password' => $enkripsi]);
    
    //isi name dengan nama
    $request->merge([
        'foto' => 'user.jpg',
        'name' => $request->name,
    'id_role' => $request->hak_akses
    ]);
     
    
    //buat data User
    $dataUser = User::create($request->all());
 
    return redirect()->route("user.index")->with(
    "success",
    "Data berhasil disimpan."
     ); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::findOrFail($id);
        $data->role = Role::get();
        
        return view("user.edit", $data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validasi inputan
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'hak_akses' => 'required',
            ]);
           
            $dataUser = User::findOrFail($id);
            $dataUser->name = $request->name;
            $dataUser->email = $request->email;
            $dataUser->id_role = $request->hak_akses;
    
            //jika password tidak kosong
            if ($request->password!="") {
            $enkripsi = Hash::make($request->password);
            $dataUser->password = $enkripsi;
            }
            $dataUser->save();
        
        return redirect()->route("user.index")->with(
        "success",
        "Data berhasil diubah."
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dataUser = User::findOrFail($id);
        $dataUser->delete();


    }
}
