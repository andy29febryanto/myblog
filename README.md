
## About Myblog

System Requirements:

- Laravel 7.*
- PHP 7.2.7
- Composer 1.8.6
- Mysql / MariaDB

## Plugins
- Laravel/Jeroennoeten
- Jquery
- Datatables
- Ckeditor
- Bootstrap 4 / AdminLTE

## Database
- File : myblog_db.sql

## Login
- User Admin
Email : admin@gmail.com ; Password : adminadmin ;
- User editor
Email : andy29febryanto@gmail.com ; Password : editor123
- User penulis
Email : febriantoandi751@gmail.com ; Password : adminadmin

## Status Artikel
1. Publish
Untuk Artikel yang telah di confirm oleh editor untuk dipublikasikan.
2. Waiting List
Untuk Artikel yang telah dikirim oleh penulis, menunggu untuk konfirmasi dari editor
3. Reject
Untuk Artikel yang ditolak oleh editor
4. Draft
untuk Artikel yang masih belum selesai dikerjakan oleh penulis