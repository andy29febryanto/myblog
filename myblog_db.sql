-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 28 Jul 2020 pada 13.49
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myblog_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `artikel`
--

CREATE TABLE `artikel` (
  `id_artikel` int(10) UNSIGNED NOT NULL,
  `id_penulis` bigint(20) UNSIGNED NOT NULL,
  `id_editor` bigint(20) UNSIGNED NOT NULL,
  `id_kategori` bigint(20) UNSIGNED NOT NULL,
  `judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug_judul` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `isi_artikel` text COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `status` set('Publish','Waiting List','Reject','Draft') COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `artikel`
--

INSERT INTO `artikel` (`id_artikel`, `id_penulis`, `id_editor`, `id_kategori`, `judul`, `slug_judul`, `isi_artikel`, `email_verified_at`, `status`, `keterangan`, `created_at`, `updated_at`) VALUES
(2, 3, 2, 9, 'Autem odit omnis consequatur architecto repellendus voluptatum eaque. Excepturi quia nam minima adipisci velit non et. Molestiae illo totam impedit quidem ad.', 'perferendis-officiis-iure-officiis-numquam-est-nesciunt-est-laborum', 'Fugiat porro voluptatem sint et culpa placeat. Quaerat reprehenderit corporis atque deleniti laborum perferendis. Et et eaque ut quidem a qui. Molestiae amet unde quibusdam ipsam magnam placeat aliquam quo.', NULL, 'Publish', NULL, '2020-07-26 10:08:11', '2020-07-26 10:08:11'),
(3, 3, 2, 1, 'Porro eos consequatur numquam voluptatem. Facere excepturi excepturi dolor. Minus dolores pariatur eaque odit commodi eligendi eaque. Perferendis et molestiae a et eum.', 'at-natus-aspernatur-accusamus-et', 'Blanditiis quod nihil fugit minima id adipisci et officiis. Accusamus fugiat ab omnis. Quam in qui sapiente consequatur.', NULL, 'Publish', NULL, '2020-07-26 10:08:11', '2020-07-26 10:08:11'),
(4, 3, 2, 1, 'Explicabo voluptate est autem eveniet sint aut voluptatem sequi. Ad dolorem esse eum minima suscipit et ipsum. Soluta et expedita ratione minima repellat quasi velit.', 'esse-qui-ut-quibusdam-in-ipsum-quo-ut', 'Dolorem quaerat quia numquam vitae doloribus. Ipsam quis et et nobis facere delectus et. Vel voluptates necessitatibus incidunt animi rerum. Quia et optio asperiores reiciendis dolorem recusandae.', NULL, 'Publish', NULL, '2020-07-26 10:08:11', '2020-07-26 10:08:11'),
(5, 3, 2, 1, 'Numquam accusantium autem consectetur et sunt ad repudiandae amet. Quidem adipisci adipisci ducimus assumenda.', 'consequatur-soluta-aut-sint-nemo-ipsa-dolorum-optio', 'Molestias sed aspernatur ut similique tempore. Eius aperiam aut quasi tempora. Consequatur sit eum iusto qui veniam. Quod nam autem ratione omnis voluptatem.', NULL, 'Publish', NULL, '2020-07-26 10:08:11', '2020-07-26 10:08:11'),
(6, 3, 2, 1, 'Facilis aliquam ex porro enim. Aut id quis sed et voluptate in ut. Odit doloribus harum deserunt ut quaerat ut consequatur. Molestiae autem nulla nihil iusto.', 'temporibus-veniam-eum-commodi-praesentium', 'Quis ea et nihil inventore sed nemo dolor. Quia quia adipisci blanditiis est. Provident incidunt eum quam dolorem. Velit possimus necessitatibus nemo mollitia quod.', NULL, 'Publish', NULL, '2020-07-26 10:08:11', '2020-07-26 10:08:11'),
(7, 3, 2, 1, 'Tempora pariatur nisi facilis quo dolorum. Deleniti ut beatae est. Ut qui non sed. Quis veniam mollitia laboriosam aut qui.', 'ducimus-est-minus-et-praesentium', 'Soluta sed et dolore quisquam nihil et nemo. Minus nihil omnis neque similique qui pariatur fugit aspernatur. Delectus voluptates accusantium possimus. Et eligendi tempora consequatur illum.', NULL, 'Publish', NULL, '2020-07-26 10:08:12', '2020-07-26 10:08:12'),
(8, 3, 2, 1, 'Voluptate maxime qui et voluptatem. Inventore asperiores quae iusto id. Nam id est non ut. Facere quae ipsa esse soluta et consequatur et. Sequi magnam repellat voluptatem.', 'at-voluptates-aut-ea-minima-reiciendis-voluptatem', 'Voluptas autem a ipsa occaecati nihil itaque aliquid. Eveniet animi optio et rerum et omnis assumenda. Voluptatem minus facilis labore natus. Soluta autem non similique ab ut pariatur.', NULL, 'Publish', NULL, '2020-07-26 10:08:12', '2020-07-26 10:08:12'),
(9, 3, 2, 1, 'Totam recusandae maxime qui tenetur atque est est. In enim quidem nisi perspiciatis. Minima sunt eum eos inventore voluptas quisquam. Dicta est velit aut et fugit.', 'hic-dolorum-dignissimos-qui-quia-qui', 'Nemo aliquam ipsum aut aut qui maiores non. Sed rem ipsum cupiditate ut reiciendis.', NULL, 'Publish', NULL, '2020-07-26 10:08:12', '2020-07-26 10:08:12'),
(10, 3, 2, 1, 'Necessitatibus est quibusdam sit nesciunt porro sed necessitatibus. Dolore nulla voluptas tempore tempore.', 'sit-aliquam-consectetur-totam-qui', 'Aperiam enim ducimus cum qui. Explicabo id et eos unde ut. Alias excepturi suscipit et praesentium.', NULL, 'Publish', NULL, '2020-07-26 10:08:12', '2020-07-26 10:08:12'),
(11, 3, 2, 1, 'Facere repellat iste expedita nisi. Quod aut alias aperiam amet.', 'non-dolores-voluptas-corrupti-qui-illum-ea-fuga', 'Hic delectus at similique eos vitae possimus. Recusandae quod dolorum at voluptatem et est. Unde rem sed cumque quod omnis.', NULL, 'Publish', NULL, '2020-07-26 10:08:12', '2020-07-26 10:08:12'),
(12, 3, 2, 1, 'Eaque accusamus quasi quia quia est delectus. Dolorum eveniet tempore et veritatis totam ad et. Tenetur fugiat quae sed vero. Velit porro quis sit laboriosam et atque nesciunt et.', 'sit-quae-aperiam-quo-consequatur-sed', 'Ea quis necessitatibus qui similique repudiandae. Minus consectetur exercitationem assumenda ut atque. Modi et natus et unde. Est maxime mollitia praesentium voluptatem.', NULL, 'Reject', NULL, '2020-07-26 10:09:07', '2020-07-26 10:09:07'),
(13, 3, 2, 1, 'Id distinctio reprehenderit nam non. Minima est ipsum libero qui. Explicabo quia omnis dolorem necessitatibus repudiandae. Omnis reiciendis temporibus quae dolores recusandae dolorem.', 'dolor-aperiam-veniam-qui-et-ut', 'Consequuntur amet assumenda voluptatem nulla. Voluptatem beatae sit eaque et. Harum dolorum minima molestias est. Cumque ut nisi minus sit.', NULL, 'Reject', NULL, '2020-07-26 10:09:07', '2020-07-26 10:09:07'),
(14, 3, 2, 1, 'Illum eos id quo nulla. Optio accusantium in blanditiis. Beatae ut illo ad. Et neque nobis dolor necessitatibus et quia ut. Rerum eos quod aut ut error commodi.', 'exercitationem-corporis-placeat-illum-vel-nam-quo-qui', 'Aut vel voluptas quia voluptates rerum error reiciendis. Numquam alias consequuntur autem omnis. Et ad laborum consequatur adipisci eum. Unde aperiam possimus minus tenetur.', NULL, 'Reject', NULL, '2020-07-26 10:09:07', '2020-07-26 10:09:07'),
(15, 3, 2, 1, 'Autem explicabo suscipit pariatur itaque voluptates commodi molestiae. Doloremque alias accusantium id sint maxime. Dicta alias et totam et necessitatibus aliquam.', 'ut-accusantium-amet-velit-omnis-neque-dolorem-dolor', 'Aliquid eaque facere ut nihil tenetur ab. Doloremque earum maiores excepturi et et et omnis quibusdam. Debitis molestiae ratione deleniti possimus.', NULL, 'Reject', NULL, '2020-07-26 10:09:07', '2020-07-26 10:09:07'),
(16, 3, 2, 1, 'Nihil error non voluptatem dolor. Dolores sunt dolores non. At qui tempore earum.', 'a-voluptatem-autem-velit-quasi-animi', 'Ullam laborum necessitatibus nihil reiciendis ut harum. Architecto sed occaecati est ab vero. Ut nesciunt aut laborum.', NULL, 'Reject', NULL, '2020-07-26 10:09:07', '2020-07-26 10:09:07'),
(17, 3, 2, 1, 'Alias quis autem adipisci adipisci ex ipsa nesciunt explicabo. Rerum eveniet recusandae quis blanditiis qui unde. Aperiam ex aliquid et voluptate autem.', 'numquam-ullam-dolores-esse-ut-repellendus-et-quasi', 'Excepturi velit et laboriosam placeat. Qui molestiae sapiente voluptatem minima nostrum voluptatibus possimus sunt.', NULL, 'Reject', NULL, '2020-07-26 10:09:07', '2020-07-26 10:09:07'),
(18, 3, 2, 1, 'Minima quia et quasi sed. Fugit rerum voluptates aut quisquam eius. Minima neque maxime sed voluptatem. Illo maiores nulla atque.', 'et-qui-ut-accusantium-cupiditate-assumenda-ad', 'Vel ut at laborum est rerum. Debitis vero porro omnis nobis blanditiis explicabo. Iure ipsum minima sint ut in aspernatur. Veritatis odio ut voluptas et.', NULL, 'Reject', NULL, '2020-07-26 10:09:07', '2020-07-26 10:09:07'),
(19, 3, 2, 1, 'Qui doloremque quod exercitationem aliquam et ut fugiat. Quae odio consequuntur eligendi maiores autem. Possimus perspiciatis rem est sit sint enim voluptatibus qui.', 'alias-dolorem-velit-aliquam-hic-fuga-cumque-qui', 'Aspernatur dicta animi perspiciatis quod fugiat id quia quibusdam. In voluptatibus officiis aut ut. Occaecati autem animi vitae eligendi modi.', NULL, 'Reject', NULL, '2020-07-26 10:09:07', '2020-07-26 10:09:07'),
(20, 3, 2, 1, 'Et deleniti consequuntur sit ea id dolor. Voluptatem cumque nesciunt reiciendis perspiciatis. Amet nihil in nobis.', 'quis-ut-voluptatem-veniam-voluptate-facere', 'Nisi quia in rerum suscipit aut mollitia deleniti. Adipisci mollitia aut sunt minus nulla fuga quia molestias. Sequi necessitatibus qui magni a laborum quisquam. Magni aut est corporis accusamus possimus sunt.', NULL, 'Reject', NULL, '2020-07-26 10:09:07', '2020-07-26 10:09:07'),
(21, 3, 2, 1, 'Omnis aut odit sit velit. Sed sapiente nobis hic non numquam corporis mollitia. Quis labore sit aut.', 'eveniet-magni-sunt-ratione-est-nihil-voluptas-enim', 'Labore provident reiciendis natus dolore facere vitae. Totam alias eos expedita quisquam. Laudantium maiores qui iure vero architecto minima.', NULL, 'Reject', NULL, '2020-07-26 10:09:07', '2020-07-26 10:09:07'),
(22, 3, 2, 1, 'Qui quod et reprehenderit repellendus et. Laudantium sed sequi libero voluptatem. Rerum aspernatur amet error doloribus sed et.', 'provident-doloremque-sapiente-magnam-ut', 'Odio minus fuga ullam voluptas. Necessitatibus consectetur accusantium hic laborum. Consequatur laborum illo commodi consequuntur omnis. Ea nesciunt laudantium voluptatum fugit et debitis tempora.', NULL, 'Waiting List', NULL, '2020-07-26 10:09:24', '2020-07-26 10:09:24'),
(23, 3, 2, 1, 'Perspiciatis dicta quas aliquam provident. Et ab consequatur quas minus et ut itaque debitis. Et quia dicta qui. Et iure dolores tempore deleniti nisi eaque est.', 'eum-aut-quidem-dolorem-voluptas-quas', 'Voluptates aliquam qui sunt odit. Laborum ea non omnis earum dicta sed. Ipsum earum et et dolorum. Id distinctio dolorem facilis quidem natus.', NULL, 'Waiting List', NULL, '2020-07-26 10:09:24', '2020-07-26 10:09:24'),
(24, 3, 2, 1, 'Iure eum suscipit eos. Aut dolor hic eum qui. Blanditiis ab velit itaque minima incidunt molestias. Nostrum facere eaque quidem ut a rerum.', 'facere-itaque-ut-animi-ut-perspiciatis', 'Dolorem non necessitatibus enim et libero explicabo. Et aut veritatis ab. Et inventore nihil in ratione tenetur omnis porro. Debitis nulla culpa assumenda quaerat.', NULL, 'Waiting List', NULL, '2020-07-26 10:09:24', '2020-07-26 10:09:24'),
(25, 3, 2, 1, 'Molestiae laborum provident maxime totam alias corrupti ut. Impedit sunt reiciendis sed. Fuga ut quam fuga possimus molestiae et odit. Cupiditate aut repudiandae fuga sunt.', 'vitae-nostrum-libero-delectus-aut-ad-architecto', 'Sunt dignissimos sunt ut repellat. Vel laboriosam quis perferendis asperiores consequuntur. Incidunt inventore reprehenderit doloremque ex.', NULL, 'Waiting List', NULL, '2020-07-26 10:09:24', '2020-07-26 10:09:24'),
(26, 3, 2, 1, 'Mollitia et sequi natus recusandae cupiditate non. Et esse eius culpa distinctio quis nemo omnis. Qui sint tempore sed voluptate vitae.', 'distinctio-fugit-magni-nihil-et', 'Maiores architecto voluptatibus porro qui et. Repellendus ipsa est illo voluptas similique debitis. Explicabo sit non excepturi consequatur modi.', NULL, 'Waiting List', NULL, '2020-07-26 10:09:24', '2020-07-26 10:09:24'),
(27, 3, 2, 1, 'Vel optio alias eos. Aut omnis impedit natus ratione non. Sint perferendis labore ratione nemo. Facere non earum corrupti et voluptas est. Accusamus ipsa sed sint sint omnis.', 'exercitationem-molestias-laudantium-est-dolores', 'Dolorem recusandae ab illo rerum totam. Repudiandae dolores ut dolores error non voluptas. Alias laboriosam nulla quam provident accusamus consequuntur. Aut esse aspernatur repudiandae quidem.', NULL, 'Waiting List', NULL, '2020-07-26 10:09:24', '2020-07-26 10:09:24'),
(28, 3, 2, 1, 'Enim praesentium quaerat est deserunt at. Sit dolorem enim praesentium aperiam asperiores modi voluptate.', 'amet-vel-odio-tempore-tempore', 'Nisi qui quisquam numquam id. Debitis eligendi nobis nulla iusto. Esse voluptatem hic natus esse iure.', NULL, 'Waiting List', NULL, '2020-07-26 10:09:24', '2020-07-26 10:09:24'),
(29, 3, 2, 1, 'Ex et necessitatibus deserunt laboriosam ducimus et. Neque ea aliquid quae architecto. Ducimus totam et consequatur exercitationem aut nisi voluptate et.', 'molestias-sed-ut-autem-odio-quam-necessitatibus', 'Sint ratione qui iure doloremque quia architecto sint. Voluptates commodi esse iste accusamus amet quidem. Suscipit fugiat corrupti occaecati.', NULL, 'Waiting List', NULL, '2020-07-26 10:09:24', '2020-07-26 10:09:24'),
(30, 3, 2, 1, 'Sapiente qui temporibus et. Temporibus qui repellat possimus tenetur aut nulla sunt. Fugit est repellat delectus quos. Nulla a autem vel id voluptate reiciendis itaque ullam.', 'esse-sunt-dolorem-cum-numquam-vitae-et-ea', 'Voluptates et debitis asperiores quis. Incidunt amet dolore voluptatum qui possimus. Quibusdam enim possimus officiis esse maiores quia.', NULL, 'Waiting List', NULL, '2020-07-26 10:09:24', '2020-07-26 10:09:24'),
(31, 3, 2, 1, 'Similique eos et nihil molestiae earum nesciunt et. Est cupiditate voluptatem est ipsum ducimus illo voluptas. Quasi pariatur aut qui qui quibusdam.', 'quod-libero-nulla-eum-eligendi-iure-nam-cum', 'Et harum dolorem voluptas et ut sed assumenda voluptas. Possimus ab est rerum omnis doloribus quae sint. Dolorem tempore iure dolorem consequuntur.', NULL, 'Waiting List', NULL, '2020-07-26 10:09:24', '2020-07-26 10:09:24'),
(32, 3, 2, 1, 'Ut ratione dolore rerum voluptatem. Aut non enim qui quibusdam quia. Velit voluptatum aut commodi eum non saepe quis.', 'aperiam-consequatur-mollitia-distinctio-occaecati-et', 'Sint praesentium est molestias et earum iste. Nesciunt dolor assumenda modi quis repudiandae rem error. Rerum eum qui aut.', NULL, 'Draft', NULL, '2020-07-26 10:09:37', '2020-07-26 10:09:37'),
(33, 3, 2, 1, 'Dolor vitae odio dolorum atque. Sit consequuntur animi nulla nulla odio aliquam et. Ratione voluptas doloremque architecto dolorem.', 'eaque-error-eum-odio-fuga-nostrum', 'Sapiente nihil voluptas sed dolorum exercitationem accusamus. Quae odio suscipit voluptatem adipisci. Molestiae sit quae quas voluptates impedit. Suscipit in quae quidem doloribus et.', NULL, 'Draft', NULL, '2020-07-26 10:09:37', '2020-07-26 10:09:37'),
(34, 3, 2, 1, 'Est et cumque ut expedita laudantium nesciunt voluptas. Eum ex minus nam architecto fugiat quidem. Ratione dolor et temporibus autem. Ipsum alias non architecto accusantium molestias atque aut.', 'natus-corrupti-natus-provident-repudiandae-magni', 'Rem veniam sed quia sapiente fuga ducimus minus. Tenetur laboriosam enim commodi harum ut omnis consequuntur maiores. Officia ipsum ad quas qui placeat.', NULL, 'Draft', NULL, '2020-07-26 10:09:37', '2020-07-26 10:09:37'),
(35, 3, 2, 1, 'Molestias corrupti consequatur quisquam rerum dolore ut. Amet repellendus est distinctio non suscipit dolorem autem. Non reiciendis ducimus quia.', 'ut-at-doloremque-ducimus-expedita-rerum', 'At ipsum iusto eos non molestias commodi et. Ullam harum autem reprehenderit itaque.', NULL, 'Draft', NULL, '2020-07-26 10:09:37', '2020-07-26 10:09:37'),
(36, 3, 2, 1, 'Labore dolorum libero enim tenetur id quae. Natus deleniti velit est in. Ut maxime consectetur autem ad doloribus consequatur. Eius libero qui voluptas et.', 'qui-ullam-ex-in-velit-reprehenderit-harum-aut', 'Sed ipsam quia ipsum voluptatem. Sapiente odio aut impedit in ut dolor. Inventore fugiat ratione et doloribus.', NULL, 'Draft', NULL, '2020-07-26 10:09:37', '2020-07-26 10:09:37'),
(37, 3, 2, 1, 'Iure repellendus officia commodi explicabo. Vero aliquam impedit in tempora. Dolorem laboriosam id labore consequatur.', 'hic-tenetur-quia-eos-id', 'Reprehenderit porro aut perspiciatis velit enim. Et sequi aut in. Voluptatibus maxime nobis eum temporibus. Ullam molestiae quia tenetur.', NULL, 'Draft', NULL, '2020-07-26 10:09:37', '2020-07-26 10:09:37'),
(38, 3, 2, 1, 'Harum sint dignissimos omnis animi. Fuga sint eum tempore. Asperiores praesentium veritatis architecto iste. Illum hic est impedit voluptas qui.', 'quia-maxime-id-laboriosam-et', 'Molestias est odit molestiae. Expedita quisquam ratione eaque nobis. Vel deserunt dolores ut illo et totam nam eius.', NULL, 'Draft', NULL, '2020-07-26 10:09:37', '2020-07-26 10:09:37'),
(39, 3, 2, 1, 'Quo odit ab architecto velit provident est esse odit. Quibusdam id accusamus et vitae. Quibusdam non similique possimus et non quisquam.', 'sequi-veniam-sit-error', 'Explicabo facere commodi omnis eius. Magni dicta corrupti laboriosam eum.', NULL, 'Draft', NULL, '2020-07-26 10:09:37', '2020-07-26 10:09:37'),
(40, 3, 2, 1, 'Qui odit praesentium est deleniti. Velit veniam quasi veritatis ea dolor non. Omnis quisquam quos explicabo sunt qui quas. Dolores laboriosam nobis ab dicta ducimus quis.', 'distinctio-similique-autem-voluptatem-et-explicabo-nemo-nam-sapiente', 'Ducimus ut molestiae dignissimos dignissimos aut ut et at. Et necessitatibus nobis dolores iure rerum recusandae. Beatae numquam fugit quidem voluptatem et rerum placeat.', NULL, 'Draft', NULL, '2020-07-26 10:09:37', '2020-07-26 10:09:37'),
(41, 3, 2, 1, 'Assumenda fugiat similique quo. Rerum eos itaque ab nihil eum. Suscipit deleniti ducimus placeat et. Quasi qui aut fuga in animi quisquam.', 'atque-assumenda-eos-culpa-architecto-molestiae-omnis', 'Architecto iusto voluptas consectetur enim nisi autem. Molestiae sit distinctio rerum velit ratione. Quia eaque amet voluptatibus non.', NULL, 'Draft', NULL, '2020-07-26 10:09:37', '2020-07-26 10:09:37'),
(42, 3, 2, 11, 'this is my latest article', 'this-is-my-latest-article', '<p>Hello my nameis andi febrianto,</p>\r\n\r\n<h1>im developer</h1>', NULL, 'Draft', NULL, '2020-07-28 01:23:48', '2020-07-28 01:23:48'),
(43, 3, 2, 1, 'Ldwaojr', 'ldwaojr', '<p>ewr35 rer</p>\r\n\r\n<p>1.</p>', NULL, 'Waiting List', NULL, '2020-07-28 03:11:38', '2020-07-28 03:11:38');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8_unicode_ci NOT NULL,
  `queue` text COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kakel`
--

CREATE TABLE `kakel` (
  `id_kategori` bigint(20) UNSIGNED NOT NULL,
  `nama_kategori` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `kakel`
--

INSERT INTO `kakel` (`id_kategori`, `nama_kategori`, `created_at`, `updated_at`) VALUES
(1, 'Ratione et voluptatem illum ut quae. Autem temporibus voluptatem quia ut porro sunt quia. Dolorum molestiae distinctio accusantium praesentium consequuntur.', '2020-07-26 09:54:46', '2020-07-26 09:54:46'),
(2, 'Pariatur in saepe suscipit et ullam et. Est exercitationem qui aperiam in autem. Recusandae suscipit ut expedita nemo ea.', '2020-07-26 09:54:46', '2020-07-26 09:54:46'),
(3, 'Dignissimos quam quo amet quidem dolorum et. Quas voluptas sunt iste perferendis deleniti. Nemo ducimus enim ea porro consequatur.', '2020-07-26 09:54:46', '2020-07-26 09:54:46'),
(4, 'Et dolore quam quis laborum voluptas facere voluptatibus. Accusantium explicabo ipsum sed ipsum quae minima ratione. Officiis et sed tenetur non. Architecto et tempora deserunt qui qui sint sapiente.', '2020-07-26 09:54:46', '2020-07-26 09:54:46'),
(5, 'Eius exercitationem consequatur nesciunt perferendis. Quo a omnis in aut vero nam et. Molestiae saepe rerum aut asperiores assumenda.', '2020-07-26 09:54:46', '2020-07-26 09:54:46'),
(6, 'Laudantium laborum sint sed exercitationem et. Ex est eum maxime perferendis nemo saepe. Iure fuga dolores numquam animi labore. Officiis omnis officia amet quia.', '2020-07-26 09:54:46', '2020-07-26 09:54:46'),
(7, 'Incidunt consequatur culpa ab ducimus. Non praesentium totam veniam accusamus. Quidem autem autem autem atque et.', '2020-07-26 09:54:46', '2020-07-26 09:54:46'),
(8, 'In fugit optio et suscipit. Velit ut ad at enim. Et aliquam iusto voluptates doloribus. Cumque expedita sint velit magni quod.', '2020-07-26 09:54:46', '2020-07-26 09:54:46'),
(9, 'Et aperiam placeat tempore. Quia in voluptatibus nesciunt veniam. Quas nihil voluptatibus impedit eum in quia rerum mollitia. Recusandae sit accusantium officiis.', '2020-07-26 09:54:46', '2020-07-26 09:54:46'),
(11, 'Ut vero dolor sequi nesciunt aut voluptatibus. Nam cupiditate quas perspiciatis qui maxime molestiae dolorem. Amet labore explicabo distinctio itaque soluta.', '2020-07-26 09:55:47', '2020-07-26 09:55:47'),
(12, 'Eum sunt id qui non. Voluptatem et accusamus officia fugit ut quis itaque. Explicabo dolorem voluptatum autem tempora et. Sunt molestias libero officia non sed.', '2020-07-26 09:55:47', '2020-07-26 09:55:47'),
(13, 'Sit esse iusto facere voluptatem consectetur non magni et. Occaecati ut ex eum. Sequi similique debitis ipsa quia sit eligendi officiis. Et et omnis modi odio nobis dolorum.', '2020-07-26 09:55:47', '2020-07-26 09:55:47'),
(14, 'Beatae illum in ea labore ex. Enim enim voluptatem nesciunt esse beatae nisi. Et quibusdam praesentium sapiente quis sed.', '2020-07-26 09:55:47', '2020-07-26 09:55:47'),
(15, 'Hic libero voluptatem voluptas voluptatem facilis reprehenderit. Occaecati ipsa est id qui rerum in quia. Eos corporis at eveniet quia.', '2020-07-26 09:55:47', '2020-07-26 09:55:47'),
(16, 'Quia temporibus eveniet nihil dolorem eum vel alias. Sed culpa voluptate a natus velit ut. Dolor ratione cum nisi numquam voluptas eos eaque.', '2020-07-26 09:55:47', '2020-07-26 09:55:47'),
(17, 'Non sapiente similique fuga porro quae non. Tempora harum ipsa doloremque temporibus quidem consequuntur. Atque assumenda deserunt ad id voluptatem minima.', '2020-07-26 09:55:47', '2020-07-26 09:55:47'),
(18, 'Ipsa aliquam rerum vel tenetur saepe. Pariatur nisi dolorum nesciunt maxime. Voluptatem ut et sint unde.', '2020-07-26 09:55:47', '2020-07-26 09:55:47'),
(19, 'Optio unde ipsa itaque eius aut unde. Blanditiis ut quia qui temporibus. Et neque quae molestias.', '2020-07-26 09:55:48', '2020-07-26 09:55:48'),
(20, 'Expedita eius iusto impedit quos. Iusto assumenda quo porro enim itaque.', '2020-07-26 09:55:48', '2020-07-26 09:55:48');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('febriantoandi751@gmail.com', '$2y$10$BClvv193/iVX3uu9A10BcelD8oL8ET40BXqsYJ22fca6xOOs.prI6', '2020-07-26 10:20:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id_role` int(10) UNSIGNED NOT NULL,
  `role_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id_role`, `role_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(2, 'editor', NULL, NULL),
(3, 'penulis', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_role` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `id_role`, `name`, `foto`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'user.jpg', 'admin@gmail.com', NULL, '$2y$10$jx6gRvotHnbRtd0soRXegexKRlEft6u90x1gkfyWAPC.Xn/DaOX/y', NULL, NULL, NULL),
(2, 2, 'andy', 'user.jpg', 'andy29febryanto@gmail.com', NULL, '$2y$10$uD5nUQPQPhezcb0LRTIcMO5iCGXr6tbWk2qpgiBBwz2J2VSlGHhLa', 'ViJrRD52wxHn0QnceVBYmHpvzdiO8HybQ6kehR4RdyELct4qgp31hHFORL4X', NULL, '2020-07-26 10:16:28'),
(3, 3, 'febrianto', 'user.jpg', 'febriantoandi751@gmail.com', NULL, '$2y$10$jx6gRvotHnbRtd0soRXegexKRlEft6u90x1gkfyWAPC.Xn/DaOX/y', '97BZlFXIGlJwgUmM39zMwZuqgASGFyg2ipXguWQEFBjHPC6yKArmQ2NMwc49', NULL, '2020-07-26 10:19:31');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id_artikel`),
  ADD KEY `fk_id_penulis` (`id_penulis`),
  ADD KEY `fk_id_kategori` (`id_kategori`),
  ADD KEY `id_editor` (`id_editor`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kakel`
--
ALTER TABLE `kakel`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_role`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_id_role_foreign` (`id_role`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id_artikel` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kakel`
--
ALTER TABLE `kakel`
  MODIFY `id_kategori` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id_role` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `artikel`
--
ALTER TABLE `artikel`
  ADD CONSTRAINT `fk_id_editor` FOREIGN KEY (`id_editor`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `fk_id_kategori` FOREIGN KEY (`id_kategori`) REFERENCES `kakel` (`id_kategori`),
  ADD CONSTRAINT `fk_id_penulis` FOREIGN KEY (`id_penulis`) REFERENCES `users` (`id`);

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_id_role_foreign` FOREIGN KEY (`id_role`) REFERENCES `roles` (`id_role`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
