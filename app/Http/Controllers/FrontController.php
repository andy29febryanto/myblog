<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artikel;
use App\User;
use App\Kakel;
class FrontController extends Controller
{
    public function index()
    {
        $data = Artikel::where('status','=','Publish')->orderBy('updated_at','desc')->limit(5)->paginate(10);
        
        // dd($data);
        return view('dashboard.home',['data'=>$data]);
    }

    public function lspenulis()
    {
        $data = User::where('id_role', 3)->get();        
        // dd($data);
        return view('dashboard.penulis',['data'=>$data]);
    }

    public function lskategori()
    {
        $data = Kakel::get();        
        // dd($data);
        return view('dashboard.kategori',['data'=>$data]);
    }

    public function show($slug)
    {
        $data = Artikel::where('slug_judul', $slug)->first();
        // dd($data);
        return view('dashboard.dtlartikel',compact('data'));
    }
    public function dtlcategory($id)
    {
        $data = Artikel::where('id_kategori', $id)->where('status','Publish')->paginate(10);
        // dd($data);
        return view('dashboard.dtlcategory',['data'=>$data]);
    }
    public function dtlwritter($id)
    {
        $penulis = User::where('id', $id)->first();
        $data = Artikel::where('id_penulis', $id)->where('status','Publish')->paginate('10');
        // dd($data);
        return view('dashboard.dtlpenulis',compact(['data','penulis']));
    }
}
