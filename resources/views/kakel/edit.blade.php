@extends('adminlte::page')
@section('title','Kategori')
@section('content_header')
    <h1 class="m-0 text-dark">Kategori Artikel</h1>
@stop
@section('content')
    <div class="row">
        <div class="col-12">
            @if ($errors->any())
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" data-dismiss="alert" class="close" aria-hidden="true">x</button>
                    <h4><i class="icon fa fa-warning"></i>Perhatian!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
            <div class="card">
                <div class="card-header">
                    Edit Kategori
                </div>
                <div class="card-body">
                    <form action="{{ route('kakel.update',$id_kategori ?? '') }}" method="post" class="form-horizontal">
                        @method('PUT')
                        {{ csrf_field() }}

<div class="form-group">
    <label for="nama" class="col-sm-2 cotrol-label">Kategori Artikel</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="nama_kategori" value="{{ $nama_kategori ?? ''  }}">
    </div>
</div>
<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        <input type="submit" class="btn btn-md btn-success" name="simpan" value="Simpan">
        <a href="{{ route('kakel.index') }}" role="button" class="btn btn-primary">Batal</a>
    </div>
</div>
                    </form>
                </div>
            </div>

        </div> 
    </div>
    @stop
    @section('plugins.Pace',true)