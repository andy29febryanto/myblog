@extends('beranda')
@section('title')
MyBlog-Dashboard    
@endsection

@section('content')
<div class="container-fluid mt-5 pt-5">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="/">Beranda</a></li>
          <li class="breadcrumb-item active" aria-current="page">Deatil Artikel</li>
        </ol>
      </nav>
    </div>
    
    <div class="container-fluid mt-3 py-3 mb-3 bg-white">
        <div class="row text-center">
            <div class="col-md">
                <h2 class="wow bounce fast judul-profil" style="color:#42F0CD"><b>Deatil Artikel</b></h2>
            </div>
        </div>
    
        <div class="row">
            <div class="col">
                <div class="card mb-3">

                    <div class="card-body">
                      <h5 class="card-title">{{ $data->judul }}</h5>
                      <h6 class="card-subtitle">{{ $data->kakel->nama_kategori }}</h6>
                      <br><p class="card-text">{!! $data->isi_artikel !!}</p>
                      <p class="card-text"><small class="text-muted">{{ Carbon\Carbon::parse($data->updated_at)->diffForHumans()}}</small></p>
                      <span class="card-link">Penulis: {{ $data->penulis->name }} </span>
                      <span class="card-link">Editor: {{ $data->editor->name }} </span>
                    </div>
                  </div>     
            </div>
        </div>
        <div class="row mb-3">
            <div class="col">
                <a href="{{ route('article') }}" class=" btn bg-danger btn-danger">Kembali</a>
    
            </div>
        </div>
    </div>
@endsection