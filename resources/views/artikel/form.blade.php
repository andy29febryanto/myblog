{{ csrf_field() }}


                        <div class="form-group">
                            <label for="judul" class="col-sm-2 cotrol-label">Judul</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="judul" value="{{ $judul ?? ''  }}">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="kategori" class="col-sm-2 cotrol-label">kategori artikel</label>
                            <div class="col-sm-10">
                                <select name="id_kategori" class="form-control">
                                    @foreach ($kakel as $item)
                                        <option value="{{ $item->id_kategori }}" {{ ( ($id_kategori ?? ''   ) == $item->id_kategori ) ? 'selected' : '' }}>
                                            {{ $item->nama_kategori }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="isi">isi</label>
                            <textarea name="isi_artikel"  cols="70" rows="7">{!! $isi_artikel ?? '' !!}</textarea>    
                        </div>


<div class="form-group">
<label for="Status">Status</label>
                    <select class="form-control" name="status">
                        <option value="Draft">
                            Draft
                        </option>
                        <option value="Waiting List">Waiting List</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="editor" class="col-sm-2 cotrol-label">Editor</label>
                    <div class="col-sm-10">
                        <select name="id_editor" class="form-control">
                            @foreach ($editor as $item)
                                <option value="{{ $item->id }}" {{ ( ($id_editor ?? ''   ) == $item->id_editor ) ? 'selected' : '' }}>
                                    {{ $item->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
<input type="hidden" class="form-control" name="emlpn" value="{{ $id_penulis ?? ''  }}">
<input type="hidden" class="form-control" name="emled" value="{{ $id_editor ?? ''  }}">
<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        <input type="submit" class="btn btn-md btn-success" name="simpan" value="Simpan">
        <a href="{{ route('artikel.index') }}" role="button" class="btn btn-primary">Batal</a>
    </div>
</div>