@extends('beranda')
@section('title')
MyBlog-Dashboard    
@endsection
@section('content')
<section>
    <div class="container">
    	<div class="row">
            <div class="col-md-12">
                <h2 style="text-align:center ;color:#42F0CD; padding-top:50px;" class="wow bounce slow"><i class="fas fa-crooshair"></i> <b>Para Penulis</b></h2>
              </div>
        </div>
        

        <div class="row">
            @forelse ($data as $item)
    		<div class="col-md-4 mt-4 mb-3 py-4">
    		    <div class="card profile-card-5">
    		        <div class="card-img-block">
    		            <img class="card-img-top" src="/assets/picture/{{$item->foto}}" alt="User Image">
    		        </div>
                    <div class="card-body pt-0">
                    <h5 class="card-title">{{ $item->name }}</h5>
                    <p class="card-text">{{ $item->email }}</p>
                    <a href="{{ route('dtlWritter',$item->id) }}" class="btn btn-primary">Lihat Artikel</a>
                  </div>
                </div>
        @empty
            Empty Data
        @endforelse

    		
    </div>            
    	</div>
    </div>
</section>
@stop