<?php

use Illuminate\Database\Seeder;
use App\Kakel;

class KakelTableSeedery extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 10;
        factory(Kakel::class, $count)->create();
    }
}
