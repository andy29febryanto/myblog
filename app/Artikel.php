<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
class Artikel extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_artikel','id_penulis','id_editor','id_kategori','judul','slug_judul','isi_artikel','status','keterangan'
    ];
    protected $table = "artikel";
    protected $primaryKey = "id_artikel";

    protected $dates = ['created_at', 'updated_at'];
    public function kakel()
    {
        return $this->belongsTo('App\Kakel', 'id_kategori', 'id_kategori');
    }
    public function penulis()
    {
        return $this->belongsTo('App\User', 'id_penulis', 'id');
    }
    public function editor()
{
    return $this->belongsTo('App\User', 'id_editor','id');
}

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
}
