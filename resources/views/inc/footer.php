

			<footer class="footer-area footer--light">

  <div class="mini-footer">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="copyright-text">
            <p>© 2020
              <a href="/">My Blog</a>. All rights reserved
            </p>
          </div>

          <div class="go_top">
            <a href="#"><span><i class="fa fa-arrow-up" style=color:white;></i></span></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>