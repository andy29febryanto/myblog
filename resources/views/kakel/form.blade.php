{{ csrf_field() }}

<div class="form-group">
    <label for="kategori artikel" class="col-sm-2 cotrol-label">Kategori Artikel</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="nama_kategori" value="{{ $nama_kategori ?? ''  }}">
    </div>
</div>

<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        <input type="submit" class="btn btn-md btn-success" name="simpan" value="Simpan">
        <a href="{{ route('kakel.index') }}" role="button" class="btn btn-primary">Batal</a>
    </div>
</div>