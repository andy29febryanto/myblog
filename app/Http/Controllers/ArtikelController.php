<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Artikel;
use App\Kakel;
use App\User;
use Notification;
use App\Notifications\WaitingList;
use App\Notifications\Congrats;
use App\Notifications\Rejected;

class ArtikelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id_role = Auth::user()->id_role;
        if ($id_role=="2") {
            $data = Artikel::whereIn('status',['Publish','Reject','Waiting List'])
            ->get();
        }else if ($id_role=='3') {
            $id = Auth::user()->id;
            $data = Artikel::where('id_penulis',$id)
            ->get();
        }
        

        $tampil['data'] = $data;

        // dd($tampil);
        return view('artikel.index', $tampil);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['artikel'] = Artikel::get();
        $data['kakel'] = Kakel::get();
        $data['editor'] = User::where('id_role',2)->get();
    //    dd($data);
            return view('artikel.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
    $iduser = Auth::user()->id;
        $this->validate($request, [
            'judul' => 'required',
            'id_kategori' => 'required',
            'isi_artikel' => 'required',
            'status' => 'required',
            ]); 
            
   //buatslug
    $slugjudul = str_slug($request->judul);
    $request->merge(['slug_judul' => $slugjudul]);
    
    //isi name dengan nama
    $request->merge([
        'id_penulis' => $iduser,
        'id_editor' => $request->id_editor,
        'judul' => $request->judul,
        'id_kategori' => $request->id_kategori,
        'isi' => $request->isi,
        'status' => $request->status,
    ]);

    if ($request->status == "Waiting List") {
        $edtreml = User::where('id', $request->id_editor)->first();
        $edtreml->notify(new WaitingList("A new Article on your application."));

    }
     
    
    //buat data User
    $dataArtikel = Artikel::create($request->all());
 
    return redirect()->route("artikel.index")->with(
    "success",
    "Data berhasil disimpan."
     ); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $data = Artikel::findOrFail($id);
        $data->penulis = User::where('id_role', 3)->get();
        $data->editor = User::where('id_role', 2)->get();
        $data->kakel = Kakel::get();
        $id_role = Auth::user()->id_role;
        if ($id_role=='3') {
            return view("artikel.edit_penulis", $data);
        }else if ($id_role=='2') {
            return view("artikel.edit_editor", $data);
        } 
       
 
        // dd($data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $iduser = Auth::user()->id;
        $this->validate($request, [
            'judul' => 'required',
            'id_kategori' => 'required',
            'isi_artikel' => 'required',
            'status' => 'required',
            ]); 
            

            $dataArtikel = Artikel::findOrFail($id);
            $dataArtikel->judul = $request->judul;
            $dataArtikel->isi_artikel = $request->isi_artikel;
            $dataArtikel->id_penulis = $request->id_penulis;
            $dataArtikel->id_editor = $request->id_editor;
            $dataArtikel->id_kategori = $request->id_kategori;
            $dataArtikel->status = $request->status;

   //buatslug
            $slugjudul = str_slug($request->judul);
            $dataArtikel->slug_judul = $slugjudul;


            if ($request->status == "Waiting List") {
                $edtreml = User::where('id', $request->id_editor)->first();
                $edtreml->notify(new WaitingList("Check New Article"));
        
            }else if ($request->status == "Publish") {
                $edtreml = User::where('id', $request->id_penulis)->first();
                $edtreml->notify(new Congrats("Good Job. Your article have been published"));
            }else if ($request->status == "Reject") {   
                $edtreml = User::where('id', $request->id_penulis)->first();
                $edtreml->notify(new Rejected("I'm Sorry, your article haven't been published. you can check your article again"));
            }
        
        return redirect()->route("artikel.index")->with(
        "success",
        "Data berhasil diubah."
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
