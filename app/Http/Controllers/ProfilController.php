<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Hash;

class ProfilController extends Controller
{
    public function index($id)
    {
        $data = User::findOrfail($id);
        return view('profil.index', $data);
    }

    public function update(Request $request, $id)
    {
            //validasi inputan
            $this->validate($request, [
                'foto' => 'required',
                'name' => ['required', 'string', 'max:255'],
                
                ]);
               
                $dataUser = User::findOrFail($id);
                $dataUser->name = $request->name;
                $dataUser->email = $request->email;
                
                if ($request->foto!=""){
                    $file = $request->file('foto');
                    $extension=$file->getClientOriginalExtension();
                    $destinationPath = 'assets/picture';
                    $fileName = rand(11111, 99999) . '.' . $extension;
                    $request->file('foto')->move($destinationPath, $fileName);
                    $dataUser->foto = $fileName;
                }

        
                //jika password tidak kosong
                if ($request->password!="") {
                $enkripsi = Hash::make($request->password);
                $dataUser->password = $enkripsi;
                }


                $dataUser->save();
            
            return redirect()->route("user.index")->with(
            "success",
            "Data berhasil diubah."
            );
    }
}
