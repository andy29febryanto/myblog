<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index')->name('article');
Route::get('/dtlarticle/{slug}', 'FrontController@show')->name('dtlArticle');
Route::get('/penulis', 'FrontController@lspenulis')->name('penulis');
Route::get('/kategori', 'FrontController@lskategori')->name('kategori');
Route::get('/dtlcategory/{kategori}', 'FrontController@dtlcategory')->name('dtlCategory');
Route::get('/dtlwritter/{penulis}', 'FrontController@dtlwritter')->name('dtlWritter');

Auth::routes();

Route::get('/home', function() {
    return view('home');
})->name('home')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['middleware' => 'auth'], function () {
    Route::resource('kakel', 'KakelController');
    Route::resource('artikel', 'ArtikelController');
    Route::resource('user', 'UserController');
    Route::resource('role', 'RoleController');
    Route::get('/profil/{profil}', 'ProfilController@index')->name('profil.index');
    Route::put('/profil/{profil}', 'ProfilController@update')->name('profil.update');
    
  });