{{ csrf_field() }}

<div class="form-group">
    <label for="nama" class="col-sm-2 cotrol-label">Nama</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="name" value="{{ $name ?? ''  }}">
    </div>
</div>

<div class="form-group">
    <label for="email" class="col-sm-2 cotrol-label">Email</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="email" value="{{ $email ?? ''  }}">
    </div>
</div>
<div class="form-group">
    <label for="password" class="col-sm-2 cotrol-label">Password</label>
    <div class="col-sm-10">
        <input type="password" class="form-control" name="password" value="{{ $password ?? ''  }}">
    </div>
</div>
<div class="form-group">
    <label for="hak_akses" class="col-sm-2 cotrol-label">Hak Akses</label>
    <div class="col-sm-10">
        <select name="hak_akses" class="form-control">
            @foreach ($hak_akses as $item)
                <option value="{{ $item->id_role }}" {{ ( ($hak_akses ?? ''   ) == $item->role_name ) ? 'selected' : '' }}>
                    {{ $item->role_name }}
                </option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        <input type="submit" class="btn btn-md btn-success" name="simpan" value="Simpan">
        <a href="{{ route('user.index') }}" role="button" class="btn btn-primary">Batal</a>
    </div>
</div>