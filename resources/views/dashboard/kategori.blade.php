@extends('beranda')
@section('title')
MyBlog-Dashboard    
@endsection
@section('content')
<section>
    <div class="container">
    	<div class="row">
            <div class="col-md-12">
                <h2 style="text-align:center ;color:#42F0CD; padding-top:50px;" class="wow bounce slow"><i class="fas fa-crooshair"></i> <b>Data seluruh kategori</b></h2>
              </div>
        </div>
        <div class="row">
@forelse ($data as $item)
    
        <div class="col-md-4">
            <div class="card-box">
                <div class="card-title">
                    <h2>Data Kategori</h2>
                    <p>{{ $item->nama_kategori }}</p>
                </div>
                <div class="card-link">
                    <a href="{{ route('dtlCategory',$item->id_kategori) }}" class="c-link">Learn More
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>
        </div>
@empty 
<span><b>Empty Data</b></span>
@endforelse
</div>
    </div>
</div>
<style type="text/css">
    .card-box {
        background: #FAFAFA;
        min-height: 300px;
        position: relative;
        padding: 30px 30px 20px;
        margin-bottom: 20px;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-direction: column;
        flex-direction: column;
        -webkit-box-pack: justify;
        -ms-flex-pack: justify;
        justify-content: space-between;
        position: relative;
        cursor: pointer;
    }

    .card-box:hover {
        background: linear-gradient(to right, #1fa2ff17 0%, #12d8fa2b 51%, #1fa2ff36 100%);
    }

    .card-box:after {
        display: block;
        background: #2196F3;
        border-top: 2px solid #2196F3;
        content: '';
        width: 100%;
        position: absolute;
        left: 0;
        right: 0;
        top: 0;
    }

    .card-title h2 {
        margin: 0;
        padding-top: 5%;
        color: #2196F3;
        font-family: 'Oswald', sans-serif;
        text-transform: uppercase;
        font-size: 24px;
        line-height: 1;
        margin-bottom: 15px;
    }

    .card-title p {
        margin: 0;
        margin-bottom: 10px;
        font-size: 16px;
    }

    .card-link a {
        text-decoration: none;
        font-family: 'Oswald', sans-serif;
        color: #FF5722;
        font-size: 15px;
    }

</style>
@stop