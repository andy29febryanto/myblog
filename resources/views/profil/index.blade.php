@extends('adminlte::page')
@section('title','Edit Profil')
@section('content_header')
    <h1 class="m-0 text-dark">Edit Profil</h1>
    @endsection
@section('content')

<div class="row">
        <div class="col-12">
            @if($errors->any())
            <div class="alert alert-warning alert-dismissable">
                <button class="close" type="button" data-dismiss="alert" aria-hidden="true">x</button>
                <h4><i class="icon fa fa-warning"></i>Perhatian!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="card">
                <div class="card-header">
                    Edit Profil
                </div>
                <div class="card-body">
                    <form action = "{{ route('profil.update', $id ?? '') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">Foto </label>
                            <div class="col-lg-9 col-xl-6">
                                <img class="kt-avatar__holder" src="/assets/picture/{{$foto}}" id="showgambar" style="height:300px;" alt="">
                                <input type="file" id="inputgambar" name="foto" value="{{$foto}}" class="validate"/ >
                            </div>
                        </div>
                          <div class="form-group">
                            <label for="name" class="col-sm-2 cotrol-label">Nama</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="name" value="{{ $name ?? ''  }}">
                            </div>
                        </div>
                        <div class="form-group">
                          <label for="email" class="col-sm-2 cotrol-label">Email</label>
                          <div class="col-sm-10">
                              <input type="email" class="form-control" name="email" value="{{ $email ?? ''  }}">
                          </div>
                      </div>
                      
                      <div class="form-group">
                        <label for="password" class="col-sm-2 cotrol-label">password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" placeholder="Jika tidak ingindiubah,tidak perlu diisi" name="password" value="{{ $password ?? ''  }}">
                        </div>
                    </div>

                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-2">
                                <input type="submit" class="btn btn-md btn-success" name="simpan" value="Simpan">
                                <a href="{{ route('profil.update', $id ?? '') }}" role="button" class="btn btn-primary">Batal</a>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>

        </div>
    </div>

 @endsection
 @section('plugins.Pace', true)
 @section('js')
 <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

 <script type="text/javascript">

    function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#showgambar').attr('src', e.target.result);
          }

          reader.readAsDataURL(input.files[0]);
      }
  }

  $("#inputgambar").change(function () {
      readURL(this);
  });


</script>
